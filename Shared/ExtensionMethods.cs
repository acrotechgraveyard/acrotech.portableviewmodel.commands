﻿using System.Windows.Input;

namespace Acrotech.PortableViewModel.Commands.NetFX
{
    /// <summary>
    /// Extension methods to support simplification of System.Windows.Input.ICommand function calls
    /// </summary>
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Calling CanExecute without any parameters (virtualizes calling CanExecute with null parameter)
        /// </summary>
        /// <param name="command">Source command</param>
        /// <returns>Result of original CanExecute</returns>
        public static bool CanExecute(this ICommand command)
        {
            return command.CanExecute(null);
        }

        /// <summary>
        /// Calling Execute without any parameters (virtualizes calling Execute with null parameter)
        /// </summary>
        /// <param name="command">Source command</param>
        public static void Execute(this ICommand command)
        {
            command.Execute(null);
        }
    }
}
