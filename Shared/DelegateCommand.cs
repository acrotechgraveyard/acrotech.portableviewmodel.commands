﻿using System;

namespace Acrotech.PortableViewModel.Commands.NetFX
{
    /// <summary>
    /// Weak typed DelegateCommand that implements the System.Windows.Input.ICommand interface
    /// </summary>
    public class DelegateCommand : Acrotech.PortableViewModel.Commands.DelegateCommand, System.Windows.Input.ICommand
    {
        /// <inheritdoc/>
        public DelegateCommand(Action execute, Func<bool> canExecute = null)
            : base(execute, canExecute)
        {
        }

        /// <inheritdoc/>
        public DelegateCommand(Action<object> execute, Func<object, bool> canExecute = null)
            : base(execute, canExecute)
        {
        }
    }

    /// <summary>
    /// Strong typed DelegateCommand that implements the System.Windows.Input.ICommand interface
    /// </summary>
    public class DelegateCommand<T> : Acrotech.PortableViewModel.Commands.DelegateCommand<T>, System.Windows.Input.ICommand
    {
        /// <inheritdoc/>
        public DelegateCommand(Action<T> execute, Func<T, bool> canExecute = null)
            : base(execute, canExecute)
        {
        }
    }
}
