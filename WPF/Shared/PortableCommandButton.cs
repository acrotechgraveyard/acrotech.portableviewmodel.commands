﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Acrotech.PortableViewModel.Commands.WPF.Net45
{
    public class PortableCommandButton : Button
    {
        private static readonly PropertyInfo CanExecutePropertyInfo = typeof(ButtonBase).GetProperty("CanExecute", BindingFlags.Instance | BindingFlags.NonPublic);

        protected virtual bool CanExecute
        {
            get { return (bool)CanExecutePropertyInfo.GetValue(this); }
            set { CanExecutePropertyInfo.SetValue(this, value); }
        }

        protected override bool IsEnabledCore
        {
            get
            {
                return base.IsEnabledCore && CanExecute;
            }
        }

        public ICommand PortableCommand
        {
            get { return (ICommand)GetValue(PortableCommandProperty); }
            set { SetValue(PortableCommandProperty, value); }
        }

        public static readonly DependencyProperty PortableCommandProperty =
            DependencyProperty.Register("PortableCommand", typeof(ICommand), typeof(PortableCommandButton), new PropertyMetadata(null, OnPortableCommandChanged));

        private static void OnPortableCommandChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((PortableCommandButton)sender).OnPortableCommandChanged((ICommand)e.OldValue, (ICommand)e.NewValue);
        }

        protected virtual void OnPortableCommandChanged(ICommand oldValue, ICommand newValue)
        {
            if (oldValue != null)
            {
                UnHookPortableCommand(oldValue);
            }

            if (newValue != null)
            {
                HookPortableCommand(newValue);
            }
        }

        protected virtual void HookPortableCommand(ICommand cmd)
        {
            HookEvent(cmd, x => cmd.CanExecuteChanged += OnPortableCommandCanExecuteChanged);
            UpdateCanExecute();
        }

        protected virtual void HookEvent<T>(T cmd, Action<T> addHandlerAction)
        {
            addHandlerAction(cmd);
        }

        protected virtual void UnHookPortableCommand(ICommand cmd)
        {
            HookEvent(cmd, x => cmd.CanExecuteChanged -= OnPortableCommandCanExecuteChanged);
            UpdateCanExecute();
        }

        protected virtual void UnHookEvent<T>(T cmd, Action<T> removeHandlerAction)
        {
            removeHandlerAction(cmd);
        }

        protected virtual void OnPortableCommandCanExecuteChanged(object sender, EventArgs e)
        {
            UpdateCanExecute();
        }

        protected virtual void UpdateCanExecute()
        {
            if (PortableCommand != null)
            {
                CanExecute = PortableCommand.CanExecute(CommandParameter);
            }
            else
            {
                CanExecute = true;
            }
        }
    }
}
