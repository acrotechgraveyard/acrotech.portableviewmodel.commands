SET MSBUILD="C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
SET TT="%CommonProgramFiles(x86)%\microsoft shared\TextTemplating\12.0\TextTransform.exe"

%TT% "Properties\VersionDetails.tt" -a !!ManualBuild!True

%MSBUILD% Acrotech.PortableViewModel.Commands.NoSL5.csproj /t:Rebuild /p:Configuration=Release /p:Platform=AnyCPU

DEL *.nupkg

..\..\.nuget\NuGet.exe Pack -Prop Configuration=Release -IncludeReferencedProjects

PAUSE
