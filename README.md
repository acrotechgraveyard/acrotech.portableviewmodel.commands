﻿# README #

This library is intended to provide the most minimal portable delegate command possible, providing only a basic ICommand implementation with some helper extensions, as well as a simple cancel command args class.

This library does not provide System.Windows.Input.ICommand support (due to incompatibilities with SL5 and WP8). You have two options to handle commands generically. You can use the compatible ICommand interface built into this library (identical to the NetFX version), or you can reference one of the additional libraries (SL5 or NoSL5) to provide extended delegate commands that additionally implement the NetFX ICommand interface. If you choose the univerally portable path, you can additionally reference the WPF extensions to allow binding a Button's (Portable)Command to a portable delegate command. If you choose the SL5 or NonSL5 path, then you can simply use the existing Button commanding in the WPF framework but you can only target PCL profile 259 at best.

It is recommended that you also use Acrotech.PortableViewModel to provide property notification support.

### What is this repository for? ###

* Anyone who is writing a cross platform application and wants to use MVVM but does not want to bring in a heavy view mdoel architecture.

### How do I get set up? ###

0. Include this library
0. Add DelegateCommands to your (View)Model
0. Call the ICommand functions from your view

### Who do I talk to? ###

* Contact me for suggestions, improvements, or bugs

### Changelog ###

#### 1.1.0.0 ####

* Major shift towards universal portability
    * Universal (Profile 344) commanding without support of System.Windows.Input.ICommand
    * SL5 and NoSL5 (Profile 259) assemblies that individually support System.Windows.Input.ICommand
    * WPF portable button control to support direct command binding

#### 1.0.1.0 ####

* Refactoring to support optional parameters
* Better handling of null delegates (no exceptions thrown)
* Usings cleanup
* Fully unit tested!
* Adding CF35 support
* Updating PCL profile to 259 (better compatibility)
    * Temporarily dropping SL5 support due to incompatibilty of ICommand with WP SL

#### 1.0.0.0 ####

* Initial Release
