﻿namespace Acrotech.PortableViewModel.Commands
{
    /// <summary>
    /// Basic Cancel Command Args. 
    /// 
    /// These args can be used to extract a cancellation of the command after executing by the caller of the command.
    /// </summary>
    public class CancelCommandArgs
    {
        /// <summary>
        /// Create a new Cancel Command Args instance with the <paramref name="initialIsCancelRequestedValue"/> initial IsCancelRequested value
        /// </summary>
        /// <param name="initialIsCancelRequestedValue">Initial Cancel value (false by default)</param>
#if PocketPC
        public CancelCommandArgs(bool initialIsCancelRequestedValue)
#else
        public CancelCommandArgs(bool initialIsCancelRequestedValue = false)
#endif
        {
            IsCancelRequested = initialIsCancelRequestedValue;
        }

        /// <summary>
        /// Result whether a cancel has been requested or not
        /// </summary>
        public bool IsCancelRequested { get; set; }
    }
}
