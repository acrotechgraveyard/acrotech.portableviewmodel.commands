﻿using System;

namespace Acrotech.PortableViewModel.Commands
{
    /// <summary>
    /// Basic Parameterless Delegate Command
    /// </summary>
    public class DelegateCommand : ICommand
    {
        /// <summary>
        /// Creates a new parameterless DelegateCommand
        /// </summary>
        /// <param name="execute">Delegate to execute</param>
        /// <param name="canExecute">Delegate to check if CanExecute (null by default)</param>
        /// <remarks>If <paramref name="canExecute"/> is null, then CanExecute always returns true</remarks>
#if PocketPC
        public DelegateCommand(Action execute, Func<bool> canExecute)
#else
        public DelegateCommand(Action execute, Func<bool> canExecute = null)
#endif
            : this(execute == null ? (Action<object>)null : _ => execute(), canExecute == null ? (Func<object, bool>)null : _ => canExecute())
        {
        }

        /// <summary>
        /// Creates a new DelegateCommand using <paramref name="canExecute"/> for CanExecute's return value
        /// </summary>
        /// <param name="execute">Delegate to execute</param>
        /// <param name="canExecute">Delegate to check if CanExecute (null by default)</param>
        /// <remarks>If <paramref name="canExecute"/> is null, then CanExecute always returns true</remarks>
#if PocketPC
        public DelegateCommand(Action<object> execute, Func<object, bool> canExecute)
#else
        public DelegateCommand(Action<object> execute, Func<object, bool> canExecute = null)
#endif
        {
            ExecuteAction = execute;
            CanExecuteAction = canExecute;
        }

        /// <summary>
        /// Execution Delegate
        /// </summary>
        public Action<object> ExecuteAction { get; private set; }

        /// <summary>
        /// CanExecute Delegate
        /// </summary>
        public Func<object, bool> CanExecuteAction { get; private set; }

        /// <summary>
        /// Invokes the CanExecuteChanged notification
        /// </summary>
        public virtual void RaiseCanExecuteChanged()
        {
            RaiseCanExecuteChanged(CanExecuteChanged);
        }

        /// <summary>
        /// Invokes the CanExecuteChanged notification using the provided <paramref name="canExecuteChanged"/> only if <paramref name="canExecuteChanged"/> is non-null
        /// </summary>
        /// <param name="canExecuteChanged">Notification handler to invoke</param>
        /// <remarks>This is the last function call before the notification is invoked</remarks>
        protected virtual void RaiseCanExecuteChanged(EventHandler canExecuteChanged)
        {
            if (canExecuteChanged != null)
            {
                canExecuteChanged(this, EventArgs.Empty);
            }
        }

        #region ICommand Members

        /// <inheritdoc/>
        public event EventHandler CanExecuteChanged;

        /// <inheritdoc/>
        /// <remarks>Returns true if no CanExecute delegate was provided at construction</remarks>
        public virtual bool CanExecute(object parameter)
        {
            return (CanExecuteAction == null) ? true : CanExecuteAction(parameter);
        }

        /// <inheritdoc/>
        /// <remarks>Will ignore calls to execute if the ExecuteAction is null</remarks>
        public virtual void Execute(object parameter)
        {
            if (ExecuteAction != null)
            {
                ExecuteAction(parameter);
            }
        }

        #endregion
    }

    /// <summary>
    /// Basic Delegate Command With a Strongly Typed Parameter
    /// </summary>
    /// <typeparam name="T">Type of Command Parameter</typeparam>
    public class DelegateCommand<T> : DelegateCommand, ICommand<T>
    {
        /// <summary>
        /// Creates a new strongly typed parameter DelegateCommand
        /// </summary>
        /// <param name="execute">Delegate to execute</param>
        /// <param name="canExecute">Delegate to check if CanExecute (null by default)</param>
        /// <remarks>If <paramref name="canExecute"/> is null, then CanExecute always returns true</remarks>
#if PocketPC
        public DelegateCommand(Action<T> execute, Func<T, bool> canExecute)
#else
        public DelegateCommand(Action<T> execute, Func<T, bool> canExecute = null)
#endif
            : base(execute == null ? (Action<object>)null : x => execute((T)x), canExecute == null ? (Func<object, bool>)null : x => canExecute((T)x))
        {
        }

        /// <inheritdoc/>
        public override bool CanExecute(object parameter)
        {
            return CanExecute((T)parameter);
        }

        /// <inheritdoc/>
        public override void Execute(object parameter)
        {
            Execute((T)parameter);
        }

        #region ICommand<T> Members

        /// <summary>
        /// Generic CanExecute entry point
        /// </summary>
        /// <param name="parameter">Command parameter</param>
        /// <returns>True if the command can execute, false otherwise</returns>
        /// <remarks>Calls base.Execute(parameter) by default</remarks>
        public virtual bool CanExecute(T parameter)
        {
            return base.CanExecute(parameter);
        }

        /// <summary>
        /// Generic Execute entry point
        /// </summary>
        /// <param name="parameter">Command parameter</param>
        /// <remarks>Calls base.Execute(parameter) by default</remarks>
        public void Execute(T parameter)
        {
            base.Execute(parameter);
        }

        #endregion
    }
}
